package speech.sv.cmu.edu.speechrecognizerexample;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import speech.sv.cmu.edu.service.ASRService;
import speech.sv.cmu.edu.service.GoogleASRService;

public class SpeechActivity extends Activity {
	
	protected SpeechFragment _fragment;
	protected ASRService _service;
	protected ASRResultReceiver _receiver;
	private volatile boolean _isBound = false;
	protected long s_time;
	protected long e_time;
	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	    	 // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	    	_service = ((ASRService.LocalBinder)service).getService();
	    }
	    
	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	        _service = null;
	    }
	};
	
	
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		 MenuInflater inflater = getMenuInflater();
		 inflater.inflate(R.menu.speech, menu);
		 return true;
		 
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		return super.onOptionsItemSelected(item);
	}
	
	private void doBindGoogleService() {
		_receiver = new ASRResultReceiver(null);
		Log.i(TAG, "Google service" + _receiver.toString());
		Intent asrIntent = new Intent(this, GoogleASRService.class);
		asrIntent.putExtra(ASRService.ASR_RECEIVER, _receiver);
		asrIntent.putExtra(android.speech.RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		bindService(asrIntent, mConnection, Context.BIND_AUTO_CREATE);
		_isBound = true;
	}
	
	
	private void doUnbindService() {
		if (_isBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        _isBound = false;
	    }
	}
	
	private static final String TAG = "MainActivity";
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Bind Google ASR Service
		doBindGoogleService();
		setContentView(R.layout.activity_speech);
		FragmentManager _manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = _manager.beginTransaction();
        _fragment = (SpeechFragment) _manager.findFragmentById(R.id.speech_fragment);
//        _fragment = new SpeechFragment();
//        fragmentTransaction.add(R.id.speech_fragment, _fragment);
        fragmentTransaction.commit();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		doUnbindService();
	}
	
	
	class ASRResultReceiver extends ResultReceiver {
		public ASRResultReceiver(Handler handler) {
			super(handler);
		}
		
		@Override
		protected void onReceiveResult(int resultCode, Bundle resultBundle) {
			String result;
			switch(resultCode) {
			case ASRService.ON_BIND:
				Log.i(TAG, "Result: On Bind");
				break;
			case ASRService.ON_UNBIND:
				Log.i(TAG, "Result: On UnBind");
				break;
			case ASRService.ON_READY_FOR_SPEECH:
				Log.i(TAG, "Result: On Ready for Speech");
				break;
			case ASRService.ON_BEGINNING_FOR_SPEECH:
				Log.i(TAG, "Result: On Begin of Speech");
				break;
			case ASRService.ON_END_OF_SPEECH:
				Log.i(TAG, "Result: On end of Speech");
				_fragment.setRecording(false);
				_fragment._speakButton.setBackgroundColor(Color.GRAY);
				s_time = System.currentTimeMillis();
				break;
			case ASRService.ON_ERROR:
				Log.i(TAG, "Result: On Error");
				break;
			case ASRService.ON_PARTIAL_RESULT:
				result = (resultBundle.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION)).get(0);
				Log.i(TAG, "Result: On Partial Result:" + result);
				_fragment._textView.setText(result);
				_fragment._textView.invalidate();
				break;
			case ASRService.ON_FINAL_RESULT:
				result = (resultBundle.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION)).get(0);
				Log.i(TAG, "Result: On Final Result:" + result);
				e_time = System.currentTimeMillis();
				_fragment._textView.setText(result);
				_fragment._latency.setText("Latency:" + (e_time - s_time)  +" ms");
				_fragment._textView.invalidate();
				break;
			default:
				super.onReceiveResult(resultCode, resultBundle);
				break;
			}
		}
	}

	
	
}
