package speech.sv.cmu.edu.service;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.speech.SpeechRecognizer;

public final class GoogleASRService extends ASRService {
	private SpeechRecognizer _recognizer;
	
	private static final String TAG = "GoogleASRService";
	//private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;

	
	
	
	@Override
	public void onCreate() {
		
		_recognizer = SpeechRecognizer.createSpeechRecognizer(getApplicationContext());
		_recognizer.setRecognitionListener(_listener);
		Log.i(TAG, "Google ASR created");
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}


	@Override
	public void startSpeech(Intent intent) {
		if (_recognizer != null)
			_recognizer.startListening(intent);
		else
			Log.i(TAG, "No recognizer present");
	}




	@Override
	public void stopSpeech() {
		if (_recognizer != null)
			_recognizer.stopListening();
		else
			Log.i(TAG, "No recognizer present");
		
	}




	@Override
	public void cancelSpeech() {
		if (_recognizer != null)
			_recognizer.cancel();
		else
			Log.i(TAG, "No recognizer present");
	}




	@Override
	public Intent getIntent() {
		
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		
		// Specify the calling package to identify your application
		intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());

		//intent.putExtra(SpeechRecognizer.EXTRA_LOCAL_FILE, Environment.getExternalStorageDirectory() + "/test.raw");
		// Given an hint to the recognizer about what the user is going to say
		//There are two form of language model available
		//1.LANGUAGE_MODEL_WEB_SEARCH : For short phrases
		//2.LANGUAGE_MODEL_FREE_FORM  : If not sure about the words or phrases and its domain.
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		return intent;
	}
	
}
