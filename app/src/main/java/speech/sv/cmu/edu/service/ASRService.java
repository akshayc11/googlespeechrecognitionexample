package speech.sv.cmu.edu.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.speech.RecognitionListener;
import android.util.Log;


public abstract class ASRService extends Service {
	
	/**
	 * Constants
	 */
	public static final int ON_BIND = 0;
	public static final int ON_UNBIND = 1;
	public static final int ON_FINAL_RESULT = 2;
	public static final int ON_PARTIAL_RESULT = 3;
	public static final int ON_READY_FOR_SPEECH = 4;
	public static final int ON_BEGINNING_FOR_SPEECH = 5;
	public static final int ON_END_OF_SPEECH = 6;
	public static final int ON_ERROR = 7;
	
	public static final String ASR_RECEIVER = "ASR_RECEIVER";
	protected static final String TAG = "ASRService";
	protected String _server_ip;
	protected int _server_port;
	// private static final String TAG = "ASRService";
	/**
	 * Globals
	 */
	protected ResultReceiver _receiver;
	/**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        public ASRService getService() {
            return ASRService.this;
        }
    }
    
    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();
	
    @Override
    public void onCreate() {
    	super.onCreate();
    }
	
    @Override
	public IBinder onBind(Intent intent) {
		_receiver = intent.getParcelableExtra(ASR_RECEIVER);
		//Toast.makeText(getApplicationContext(), "Bound:" + mBinder.toString(), Toast.LENGTH_SHORT).show();
		return mBinder;
	}
	
    RecognitionListener _listener = new RecognitionListener() {

		@Override
		public void onBeginningOfSpeech() {
			
			_receiver.send(ON_BEGINNING_FOR_SPEECH, null);
			
		}

		@Override
		public void onBufferReceived(byte[] arg0) {
			
			
			
		}

		@Override
		public void onEndOfSpeech() {
			
			_receiver.send(ON_END_OF_SPEECH, null);
		}

		@Override
		public void onError(int error) {
			
			Log.i(TAG, "" + error);
			_receiver.send(ON_ERROR, null);
		}

		@Override
		public void onEvent(int arg0, Bundle arg1) {
			
			
		}

		@Override
		public void onPartialResults(Bundle partialResult) {
			
			_receiver.send(ON_PARTIAL_RESULT, partialResult);
		}

		@Override
		public void onReadyForSpeech(Bundle b) {
			
			_receiver.send(ON_READY_FOR_SPEECH, b);
			
		}

		@Override
		public void onResults(Bundle results) {
			
			_receiver.send(ON_FINAL_RESULT, results);
		}

		@Override
		public void onRmsChanged(float arg0) {
			
			
		}
		
	};
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}
	
	abstract public void startSpeech(Intent intent);
	abstract public void stopSpeech();
	abstract public void cancelSpeech();
	abstract public Intent getIntent();
	
	public void setIP(String ip) {
		this._server_ip = ip;
	}
	public void setPort(int port) {
		this._server_port = port;
	}
}
