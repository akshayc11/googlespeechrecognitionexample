package speech.sv.cmu.edu.speechrecognizerexample;



import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class SpeechFragment extends Fragment {
	
	protected Button _speakButton;
	private volatile boolean _isRecording = false;
	private SpeechActivity _act;
	protected SpeechActivity.ASRResultReceiver _receiver;
	protected TextView _textView;
	protected TextView _recognizerText;
	protected TextView _latency;
	public boolean isRecording() {
		return this._isRecording;
	}
	
	public void setRecording(boolean state) {
		this._isRecording = state;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_speech, container, false);
        _act = (SpeechActivity) getActivity();
        _speakButton = (Button) view.findViewById(R.id.button_speak);
        _recognizerText = (TextView) view.findViewById(R.id.recognizer);
        _latency        = (TextView) view.findViewById(R.id.latency);

        _recognizerText.setText("GOOGLE ASR");

        _speakButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!_isRecording) {
                    if (_act._service != null) {
                        Intent i = _act._service.getIntent();
                        _act._service.startSpeech(i);
                        setRecording(true);
                        v.setBackgroundColor(Color.RED);
                    }
                } else {
                    if (_act._service != null) {
                        _act._service.stopSpeech();
                        setRecording(false);
                        v.setBackgroundColor(Color.GRAY);
                    }
                }
            }
        });
        _textView = (TextView) view.findViewById(R.id.result_text);

        return view;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
}
