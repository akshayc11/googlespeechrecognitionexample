# Speech Recognition Example #

This is a sample application using the Google Speech Recognizer

### What is this repository for? ###

* Quick summary

This is a sample application to run speech recognition using microphone and the Google Speech Recognizer

* Version : 0.1


### How do I get set up? ###

* Summary of set up

In Android Studio:

    File-> Import Project

        Navigate to this project

* Configuration

* Dependencies

* Database configuration

* How to run tests

* Deployment instructions

### Contribution guidelines ###

* Writing tests

* Code review

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Akshay Chandrashekaran: akshay.chandrashekaran@sv.cmu.edu

* Other community or team contact: speech@sv.cmu.edu